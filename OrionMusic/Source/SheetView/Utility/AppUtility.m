//
//  AppUtility.m
//  RestPos
//
//  Created by Sarfaraj biswas on 04/02/13.
//  Copyright (c) 2013 OS. All rights reserved.
//

#import "AppUtility.h"

@implementation AppUtility

#pragma mark - validation methods
#pragma --

+(BOOL)isInputTextNullOrEmpty:(UITextField*)inputText
{
    NSString *trimmedString = [inputText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return (!inputText.text || [@"" isEqualToString:trimmedString]);
}

+(BOOL) isValidEmail:(NSString*)text
{
    NSString *regEx = @"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
    NSRange r = [text rangeOfString:regEx options:NSRegularExpressionSearch];
    if (r.location == NSNotFound) {
//        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                      message:@"Email address is invalid"
//                                                     delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [av show];
        return NO;
    }
    
    return YES;
}

+(BOOL) isValidPhoneNumber:(NSString*)text
{
    NSString *regEx = @"^\\d{10}$";
    NSString *phoneValue = text;
    
    NSRange r = [phoneValue
                 rangeOfString:regEx options:NSRegularExpressionSearch];
    if (r.location == NSNotFound) {
//        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                      message:@"Contact number must be 10 digits.\n land: (xx) xxxx-xxxx\n mobile: xxxx-xxx-xxx"
//                                                     delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [av show];
        return NO;
    }
    
    return YES;
}


+(NSString*) fullname:(NSString *)fname lname:(NSString *)lname
{
    return [NSString stringWithFormat:@"%@ %@", fname, lname];
}

+(NSString*) orderHeaderFormatedString
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+(NSString*) getOrderQuickServiceDate
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}


+(void)showAlertMessage:(NSString *)title msg:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alert show];
}

+(UIAlertView *)getAlertMessageView:(NSString *)title msg:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    return alert;
}

@end


