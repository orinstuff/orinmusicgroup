//
//  AppUtility.h
//  RestPos
//
//  Created by Sarfaraj biswas on 04/02/13.
//  Copyright (c) 2013 OS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtility : NSObject


+(BOOL)isInputTextNullOrEmpty:(UITextField*)inputText;
+(BOOL) isValidEmail:(NSString*)text;
+(BOOL) isValidPhoneNumber:(NSString*)text;

+(NSString*) numberFormatedString:(float)val;

+(NSString*) orderHeaderFormatedString;

+(NSString*) getOrderQuickServiceDate;

+(NSString*) priceFormatedString:(NSString *)val;

+(NSString*) fullname:(NSString *)fname lname:(NSString *)lname;


+(void)showAlertMessage:(NSString *)title msg:(NSString *)msg;

+(UIAlertView *)getAlertMessageView:(NSString *)title msg:(NSString *)msg;


@end


