//
//  PaymentViewController.m
//
//  Created by Alex MacCaw on 2/14/13.
//  Copyright (c) 2013 Stripe. All rights reserved.
//

#import "PaymentViewController.h"
#import "MBProgressHUD.h"
#import "GameUtility.h"
#import "AHKActionSheet.h"
#import "HRSoundManager.h"
#import "AppDelegate.h"
#import "AppUtility.h"

#define EXAMPLE_STRIPE_PUBLISHABLE_KEY @"pk_live_p5nThXXfefegoAXsH4T43rxS"
//pk_test_t7Dy4iWeorD1K7BqrGhdEJy0
//sk_test_NGXBGWyCrylIfFoysMCR6hXY
@interface PaymentViewController ()


- (void)hasError:(NSError *)error;
- (void)hasToken:(STPToken *)token;
@end

@implementation PaymentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Make Payment";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
      self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Setup save button
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Pay" style:UIBarButtonItemStyleDone target:self action:@selector(save:)];
    saveButton.enabled = NO;
    self.navigationItem.rightBarButtonItem = saveButton;
    
    // Setup checkout
    self.checkoutView = [[STPView alloc] initWithFrame:CGRectMake(15,160,290,55) andKey:EXAMPLE_STRIPE_PUBLISHABLE_KEY];
    self.checkoutView.delegate = self;
    [self.view addSubview:self.checkoutView];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.checkoutView.center = CGPointMake(self.view.center.x, 180);
}

- (void)stripeView:(STPView *)view withCard:(PKCard *)card isValid:(BOOL)valid
{
    // Enable save button if the Checkout is valid
    self.navigationItem.rightBarButtonItem.enabled = valid;
}

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)save:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self.checkoutView createToken:^(STPToken *token, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (error) {
            [self hasError:error];
        } else {
            [self hasToken:token];
        }
    }];
}

- (void)hasError:(NSError *)error
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                      message:[error localizedDescription]
                                                     delegate:nil
                                            cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                            otherButtonTitles:nil];
    [message show];
}

- (void)hasToken:(STPToken *)token
{

    NSLog(@"Token %@", token);
    [self doSubscribe:token.tokenId];
    
    /*
    NSString *totalPrice = [self.item.item_price stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSURL *url = [NSURL URLWithString:PAYMENT_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    UserSession* userSession = [UserAccessSession getUserSession];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            userSession.userId, @"user_id",
                            self.item.item_id, @"item_id",
                            totalPrice, @"amount",
                            token.tokenId, @"token",
                            @"", @"description",
                            nil];
    
    NSLog(@"Param %@", params);
    
    [httpClient postPath:@"" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError* error;
        NSArray* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"payment = %@", responseStr);
        
        if(json != nil && [json count] > 0)
        {
        
            NSDictionary* dictStatus = [json objectAtIndex:0];
            if([[dictStatus valueForKey:@"message_code"] integerValue] > 0)
            {

                [MGUtilities showAlertTitle:@"Info"
                                    message:@"Transaction successfully done."];

                
                [self.navigationController popToRootViewControllerAnimated:YES];
                
                return ;
            }
            else
            {
                [MGUtilities showAlertTitle:@"Error"
                                    message:[dictStatus valueForKey:@"message"]];
                
                
                return;
            }
        }
        
        [MGUtilities showAlertTitle:@"Error"
                            message:@"Transaction failed."];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [MGUtilities showAlertTitle:LOCALIZED(@"Network Error")
                            message:LOCALIZED(@"Error occured while connecting.")];
    }];
    */
}


- (void)doSubscribe: (NSString *)token {
    
	GameUtility* utils = [GameUtility sharedObject];
    NSString *username = utils.userProfile.userName;
    NSString *email = utils.userProfile.email;

    //. just send user_id, email and token
    //http://orin.io/stripe/charge_payment.php
	NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
						 username, @"username",
                         email, @"email",
                         token, @"token",
						 nil];
    
    [utils runPaymentWebService:@"" Param:dic Delegate:self View:self.view];
}


- (void)finishedRunningServiceDelegate:(NSDictionary *)outData {
    
    //	GameUtility* utils = [GameUtility sharedObject];
    
    NSArray *resultArr = (NSArray *)outData;
    
	NSLog(@"receivedJson: %@", outData);
    //	NSString* serviceName = outData[@"serviceName"];
	
	if([resultArr count] == 0) {
		return;
	}
    
    NSDictionary *dictItem = [resultArr objectAtIndex:0];

	if ([[dictItem objectForKey:@"message_code"] intValue] == 1) {
        //
        [[GameUtility sharedObject] setSubscription:YES];
        
        [AppUtility showAlertMessage:nil msg:@"Payment Completed Successfully."];
        
        [self btnBackClicked:nil];
	}
    else
    {
        NSString *message = [dictItem objectForKey:@"message"];
        if([message length] == 0)
            message = @"Payment failed";
        [AppUtility showAlertMessage:nil msg:message];
    }
}


@end


/*
 
 http://orin.io/stripe/charge_payment.php

user_id, email and token
 
 */
