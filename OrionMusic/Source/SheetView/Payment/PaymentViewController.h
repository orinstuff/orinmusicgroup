//
//  PaymentViewController.h
//  Stripe
//
//  Created by Alex MacCaw on 3/4/13.
//
//

#import <UIKit/UIKit.h>
#import "STPView.h"
//#import "Item.h"

@interface PaymentViewController : UIViewController <STPViewDelegate>

@property STPView* checkoutView;

@property (weak, nonatomic) IBOutlet UILabel *lbTotalPrice;

//@property(nonatomic, assign) Item* item;

- (IBAction)btnBackClicked:(id)sender;

- (IBAction)save:(id)sender;

//4242424242424242

@end