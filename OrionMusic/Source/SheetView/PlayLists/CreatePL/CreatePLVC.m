//
//  ChooseSyncOptionVC.m
//  WeBDay
//
//  Created by Sarfaraj biswas on 23/08/14.
//  Copyright (c) 2014 Mason Mei. All rights reserved.
//

#import "CreatePLVC.h"

#import "GameUtility.h"
#import "AHKActionSheet.h"
#import "HRSoundManager.h"
#import "AppDelegate.h"
#import "AppUtility.h"

@interface CreatePLVC ()

@property (assign) NSInteger messageType;
@property (assign) NSInteger netAction;

//@property(strong) UIImage *currImage;
//@property (nonatomic, strong) UIImagePickerController *imagePicker;

@end

@implementation CreatePLVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.messageType = 0;
    }
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil msgType:(NSInteger)msgType 
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.messageType = msgType;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}


- (IBAction)btnCloseClicked:(id)sender {
    
    [self.delegate itemSelected:0];
}


- (IBAction)btnSendClicked:(id)sender {
    [self createPlaylist:YES];
}

- (void)createPlaylist: (BOOL)bShowWaiting {
    self.netAction = 0;
    
	GameUtility* utils = [GameUtility sharedObject];
    

    if([self.txtFld.text length] == 0) return;
    
    [self.txtFld resignFirstResponder];
    
//    http://orin.io/webservice/webservice.php?type=create_playlist&user_id=1&title=test2

    
    NSString *playListTxt =[self.txtFld.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
	NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
						 utils.userProfile.userName, @"username",
                         playListTxt, @"title",
						 nil];

	if (bShowWaiting)
		[utils runPLWebService:@"create_playlist" Param:dic Delegate:self View:self.view];
	else
		[utils runPLWebService:@"create_playlist" Param:dic Delegate:self View:nil];
}


- (void)finishedRunningServiceDelegate:(NSDictionary *)outData {
    
    //	GameUtility* utils = [GameUtility sharedObject];
    
    NSArray *resultArr = (NSArray *)outData;
    
	NSLog(@"receivedJson: %@", outData);
    //	NSString* serviceName = outData[@"serviceName"];
	
	if([resultArr count] == 0) {
		return;
	}
    
    NSDictionary *dictItem = [resultArr objectAtIndex:0];
	int count = 0;
	if ([[dictItem objectForKey:@"status"] intValue] == 1) {
        
        if(self.messageType == 0)
        {
            [self.delegate itemSelected:1];
        }
        else
        {
            if(self.netAction == 0)
            {
                [self songToPlaylist:@""];
            }
            else
            {
                [self.delegate itemSelected:1];
            }
        }
	}
    else
    {
        NSString *message = [dictItem objectForKey:@"message"];
        [AppUtility showAlertMessage:nil msg:message];
    }
}

- (void)songToPlaylist: (NSString *)playlist_id {
    self.netAction = 1;
    
	GameUtility* utils = [GameUtility sharedObject];
    
    //playlist_id = @"1";
    NSString *music_id = self.songsId;
    
//    http://orin.io/webservice/webservice.php?type=add_music_playlist&music_id=8&playlist_id=1

	NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
						 music_id, @"music_id",
                         playlist_id, playlist_id,
						 nil];
    
    [utils runPLWebService:@"add_music_playlist" Param:dic Delegate:self View:self.view];
}


/*

- (IBAction)tapOnImage:(id)sender {
    NSLog(@"Did tap Photo!");
    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        NSLog(@"No Camera Detected!");
        [self pickPhoto];
        return;
    }

    [UIActionSheet showInView:self.view
                    withTitle:nil
            cancelButtonTitle:@"Cancel"
       destructiveButtonTitle:nil
            otherButtonTitles:@[@"Take a Photo", @"Pick from Photo Library"]
                     tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex){
//                         NSLog(@"Tapped '%@' at index %d", [actionSheet buttonTitleAtIndex:buttonIndex], buttonIndex);
                         
                         switch (buttonIndex) {
                             case 0:
                             {
                                 [self takePhoto];
                             }
                                 break;
                             case 1:
                             {
                                 [self pickPhoto];
                             }
                                 break;
                           
                             default:
                                 break;
                         }
                         
                     }];

}

-(UIImagePickerController *)imagePicker{
    if(_imagePicker == nil){
        _imagePicker = [[UIImagePickerController alloc]init];
        _imagePicker.delegate = self;
    }
    return _imagePicker;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    CGFloat side = 71.f;
    side *= [[UIScreen mainScreen] scale];
    
    UIImage *thumbnail = [image createThumbnailToFillSize:CGSizeMake(side, side)];
//    self.imagePicker.image = thumbnail;
    self.currImage = thumbnail;
    self.imageViewPhoto.image = thumbnail;
}

-(void) takePhoto{
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self.navigationController presentViewController:self.imagePicker animated:YES completion:nil];
}

-(void) pickPhoto{
    _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.navigationController presentViewController:self.imagePicker animated:YES completion:nil];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == actionSheet.cancelButtonIndex){
        return;
    }
    switch (buttonIndex) {
        case 0:
            [self takePhoto];
            break;
        case 1:
            [self pickPhoto];
            break;
    }
}
*/

@end
