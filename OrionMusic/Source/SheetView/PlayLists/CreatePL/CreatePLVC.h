//
//  ChooseSyncOptionVC.h
//  WeBDay
//
//  Created by Sarfaraj biswas on 23/08/14.
//  Copyright (c) 2014 Mason Mei. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CreatePLVCDelegate <NSObject>
-(void)itemSelected:(NSInteger )index;

@end


@interface CreatePLVC : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (weak, nonatomic) id<CreatePLVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *txtFld;
@property(nonatomic, strong) NSString *songsId;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil msgType:(NSInteger)msgType;

- (IBAction)btnCloseClicked:(id)sender;

- (IBAction)btnSendClicked:(id)sender;


@end
