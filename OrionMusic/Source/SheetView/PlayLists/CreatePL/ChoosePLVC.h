//
//  ChooseSyncOptionVC.h
//  WeBDay
//
//  Created by Sarfaraj biswas on 23/08/14.
//  Copyright (c) 2014 Mason Mei. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ChoosePLVCDelegate <NSObject>
-(void)itemChooseSelected:(NSInteger )index;

@end


@interface ChoosePLVC : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (weak, nonatomic) id<ChoosePLVCDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) NSMutableArray *arrAlbumItems;

@property(nonatomic, strong) NSString *songsId;
@property(nonatomic, strong) NSString *albumId;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil msgType:(NSInteger)msgType;

- (IBAction)btnCloseClicked:(id)sender;


@end
