//
//  ChooseSyncOptionVC.m
//  WeBDay
//
//  Created by Sarfaraj biswas on 23/08/14.
//  Copyright (c) 2014 Mason Mei. All rights reserved.
//

#import "ChoosePLVC.h"

#import "GameUtility.h"
#import "AHKActionSheet.h"
#import "HRSoundManager.h"
#import "AppDelegate.h"
#import "AppUtility.h"

@interface ChoosePLVC ()

@property (assign) NSInteger messageType;
@property (assign) NSInteger netAction;

@end

@implementation ChoosePLVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil msgType:(NSInteger)msgType 
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self loadMusic:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCloseClicked:(id)sender
{
    [self.delegate itemChooseSelected:0];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrAlbumItems count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SongCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    NSDictionary *item = [self.arrAlbumItems objectAtIndex:indexPath.row];
    
    //
    NSString *name = item[@"title"];
    cell.textLabel.text = name;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    //
    NSDictionary *item = [self.arrAlbumItems objectAtIndex:indexPath.row];

    NSString *plyListId = item[@"playlist_id"];
    
    [self songToPlaylist:plyListId];
}


#pragma --

- (void)loadMusic: (BOOL)bShowWaiting {
    self.netAction = 0;

	GameUtility* utils = [GameUtility sharedObject];
        
    //http://orin.io/webservice/webservice.php?type=album_song
    
	NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
						 utils.userProfile.userName, @"username",
						 nil];
    
    //http://orin.io/webservice/webservice.php?type=PlayList_Song&user_id=1
	if (bShowWaiting)
		[utils runPLWebService:@"PlayList_Song" Param:dic Delegate:self View:self.view];
	else
		[utils runPLWebService:@"PlayList_Song" Param:dic Delegate:self View:nil];
}


- (void)songToPlaylist: (NSString *)playlist_id {
    self.netAction = 1;
    
	GameUtility* utils = [GameUtility sharedObject];
    
    
    //playlist_id = @"1";
    NSString *music_id = self.songsId;
    NSString *album_id = self.albumId;
    //    http://orin.io/webservice/webservice.php?type=add_music_playlist&music_id=8&playlist_id=1
    //&album_id=1
	NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
						 music_id, @"music_id",
                         playlist_id, @"playlist_id",
                         album_id, @"album_id",
						 nil];
    
    [utils runPLWebService:@"add_music_playlist" Param:dic Delegate:self View:self.view];
}


- (void)finishedRunningServiceDelegate:(NSDictionary *)outData {
    
    //	GameUtility* utils = [GameUtility sharedObject];
    
    NSArray *resultArr = (NSArray *)outData;
    
	NSLog(@"receivedJson: %@", outData);
    //	NSString* serviceName = outData[@"serviceName"];
	
	if([resultArr count] == 0) {
		return;
	}
    
    NSDictionary *dictItem = [resultArr objectAtIndex:0];
	int count = 0;
	if ([[dictItem objectForKey:@"status"] intValue] == 1) {
        
        if(self.netAction == 0)
        {
            if(self.arrAlbumItems)
                [self.arrAlbumItems removeAllObjects];
            else
                self.arrAlbumItems = [NSMutableArray new];
            
            for(NSDictionary *dict in resultArr)
            {
                if(count++ == 0) continue;
                
                [self.arrAlbumItems addObject:dict];
            }
            
            [self.tableView reloadData];
        }
        else
        {
            [self.delegate itemChooseSelected:1];
        }
    }
    else
    {
        NSString *message = [dictItem objectForKey:@"message"];
        [AppUtility showAlertMessage:nil msg:message];
    }

}




@end
