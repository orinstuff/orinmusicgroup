//
//  ChartsViewViewController.h
//  Orin
//
//  Created by Finbarrs Oketunji on 10/18/13.
//
//

#import <UIKit/UIKit.h>
#import "ChartsView.h"
#import "EGORefreshTableHeaderView.h"
#import "EGORefreshTableFooterView.h"
#import "CreatePLVC.h"

@interface PLViewController : UIViewController <EGORefreshTableDelegate,UIScrollViewDelegate, CreatePLVCDelegate> {
	//EGOHeader
    EGORefreshTableHeaderView *_refreshHeaderView;
    //EGOFoot
    EGORefreshTableFooterView *_refreshFooterView;
    //
    BOOL _reloading;

}
@property (weak, nonatomic) IBOutlet UIView *viewPlay;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewCover;
@property (weak, nonatomic) IBOutlet UILabel *lbArtist;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property(nonatomic, strong) NSMutableArray *arrAlbumItems;
@property (strong) id parentController;

@property (strong, nonatomic) IBOutlet ChartsView *m_chartView;

@property (weak, nonatomic) IBOutlet UICollectionView *gridView;




- (IBAction)btnPlayClicked:(id)sender;


@end
