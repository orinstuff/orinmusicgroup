//
//  ChartsViewViewController.h
//  Orin
//
//  Created by Finbarrs Oketunji on 10/18/13.
//
//

#import <UIKit/UIKit.h>

@interface PLDtlsViewController : UIViewController{
    BOOL _reloading;

}

@property (weak, nonatomic) IBOutlet UIView *viewPlay;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewCover;
@property (weak, nonatomic) IBOutlet UILabel *lbArtist;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property(nonatomic, strong) NSDictionary *dictAlbumItem;


@property (weak, nonatomic) IBOutlet UILabel *lbNumberSongs;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lb2;


@property (weak, nonatomic) IBOutlet UIImageView *imageViewSongCover;

@property (weak, nonatomic) IBOutlet UILabel *lb1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil data:(NSDictionary *)data;

- (IBAction)btnPlayClicked:(id)sender;

- (IBAction)btnBackClicked:(id)sender;


@end
