//
//  ChartsViewViewController.m
//  Orin
//
//  Created by Finbarrs Oketunji on 10/18/13.
//
//

#import "PLDtlsViewController.h"
#import "GameUtility.h"

#import "HRSoundManager.h"
#import "AppDelegate.h"

@interface PLDtlsViewController ()
{
    NSInteger m_nCurCount;
    NSInteger selectedAlbum;
    NSInteger selectedAlbumSong;
}
@end

@implementation PLDtlsViewController

@synthesize dictAlbumItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil data:(NSDictionary *)data
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.dictAlbumItem = data;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"chart view: %f, %f, %f, %f", self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    
    [GameUtility setPhoneFrame:self.view withHeader:YES];
    
    self.viewPlay.hidden = YES;
 
    [self updateUI];
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [self stopAudio];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {

    [super viewDidUnload];
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝


-(NSArray *)getSongs
{
    return nil;
    
}


-(NSArray *)getAlbumSongs
{
    if(![[self.dictAlbumItem objectForKey:@"album_songs"] isKindOfClass:[NSArray class]])
        return nil;
    
    return [self.dictAlbumItem objectForKey:@"album_songs"];
}


-(NSDictionary *)getAlbumSongItem:(NSInteger)songIndex
{
    if(![[self getAlbumSongs] isKindOfClass:[NSArray class]])
        return nil;
    
    if([[self getAlbumSongs] count] == 0) return nil;
    
    NSDictionary *dictItem = [[self getAlbumSongs] objectAtIndex:songIndex];
    
    return dictItem;
}

#pragma gridview

-(void)updateUI
{
    
    NSString *name = self.dictAlbumItem[@"title"];
    
    NSDictionary *itemSong = [self getAlbumSongItem:0];

    NSString *artiest = @"Unknown";
    NSString *imagefile = @"";
    
    if(itemSong)
    {
        artiest = itemSong[@"artist"];
        imagefile = itemSong[@"imagefile"];
    }
    
    self.lbTitle.text = name;
    self.lbArtist.text = artiest;
    self.lbNumberSongs.text = [NSString stringWithFormat:@"Songs %d", [[self getAlbumSongs] count]];


//    UILabel *lb1 = (UILabel *)[cell viewWithTag:1001];
//    UILabel *lb2 = (UILabel *)[cell viewWithTag:1002];
//    UILabel *lb3 = (UILabel *)[cell viewWithTag:1003];
//    
//    //
    NSString* strUrl = [NSString stringWithFormat:@"%@/album/%@", SERVER_PATH, imagefile];
    [GameUtility setImageFromUrl:strUrl target:self.imageViewCover defaultImg:@"playlist-placeholder.png"];

    
    [self.tableView reloadData];
}

-(void)itemChooseSelected:(NSInteger )index
{
    [self dismissSemiModalView];
}


#pragma --

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self getAlbumSongs] count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SongCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        cell = (UITableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"SongCell" owner:self options:nil] objectAtIndex:0];
        
    }
    
    NSDictionary *itemSong = [self getAlbumSongItem:indexPath.row];

    //
    NSString *name = itemSong[@"name"];
    
    
    UILabel *lb1 = (UILabel *)[cell viewWithTag:1000];
    
    lb1.text = [NSString stringWithFormat:@"%d  %@", (indexPath.row + 1), name];

    UIButton *btn = (UIButton *)[cell viewWithTag:1001];
    btn.hidden = YES;
    
    UIButton *btn2 = (UIButton *)[cell viewWithTag:1002];
    btn2.hidden = YES;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    selectedAlbumSong = indexPath.row;
    
    [self playSongs];
}

/*
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:@"GridItemCell"
                                              forIndexPath:indexPath];
    
    //cell.layer.borderWidth = 1.0f;
    //cell.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    //
    NSDictionary *item = [self.arrAlbumItems objectAtIndex:indexPath.row];
    
    NSString *name = item[@"name"];
    NSString *artiest = item[@"artist"];
    NSString *imagefile = item[@"imagefile"];

    //
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1000];
    UILabel *lb1 = (UILabel *)[cell viewWithTag:1001];
    UILabel *lb2 = (UILabel *)[cell viewWithTag:1002];
    UILabel *lb3 = (UILabel *)[cell viewWithTag:1003];
    
    //
    NSString* strUrl = [NSString stringWithFormat:@"%@/song/image/%@", SERVER_PATH, imagefile];
    [GameUtility setImageFromUrl:strUrl target:imageView defaultImg:@""];

    lb1.text = name;
    lb2.text = artiest;
    lb3.text = @"Songs 12";

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedAlbum = indexPath.row;
    [self showAlbumSongs:indexPath.row];
    
//    NSDictionary *item = [self.arrAlbumItems objectAtIndex:indexPath.row];
//    NSString *songfile = item[@"songfile"];
//
//    NSString* ituneFile = [NSString stringWithFormat:@"%@/song/%@", SERVER_PATH, [GameUtility urlencode:songfile]];
//    
//    [self playRemoteFile:ituneFile];
}
*/


#pragma --
#pragma playSongs

-(void)playSongs
{

    NSDictionary *item = [self getAlbumSongItem:selectedAlbumSong];
    NSString *songfile = item[@"songfile"];
    
    NSString* ituneFile = [NSString stringWithFormat:@"%@/albumsong/%@", SERVER_PATH, [GameUtility urlencode:songfile]];
    
    [self playRemoteFile:ituneFile];
    
    //
    NSString *name = item[@"name"];
    NSString *artiest = item[@"artist"];
    NSString *imagefile = item[@"imagefile"];
    
    //
    NSString* strUrl = [NSString stringWithFormat:@"%@/album/%@", SERVER_PATH, imagefile];
    [GameUtility setImageFromUrl:strUrl target:self.imageViewSongCover defaultImg:@""];

    self.lb1.text = name;
    self.lb2.text = artiest;
}

- (IBAction)btnPlayClicked:(id)sender {

    if(self.btnPlay.selected)
    {
        [self pauseAudio];
        self.btnPlay.selected = NO;
    }
    else
    {
        self.btnPlay.selected = YES;

        [self resumeAudio];
    }
}

- (IBAction)btnBackClicked:(id)sender {
    
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

    [appDelegate.navigationController popViewControllerAnimated:YES];
}


#pragma --
#pragma playRemoteFile

-(void)playRemoteFile:(NSString *)songLink {
    
    self.viewPlay.hidden = NO;

    if([[HRSoundManager sharedManager] isPlaying])
        [[HRSoundManager sharedManager] stop];
    
    self.btnPlay.selected = YES;
    
    [[HRSoundManager sharedManager] startStreamingRemoteAudioFromURL:songLink andBlock:^(int percentage, CGFloat elapsedTime, CGFloat timeRemaining, NSError *error, BOOL finished) {
        
        if (!error) {
            
           // NSLog(@"music update %f, %f, %d", elapsedTime, timeRemaining, finished);
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"mm:ss"];
            
            NSDate *elapsedTimeDate = [NSDate dateWithTimeIntervalSinceNow:elapsedTime];
//            _elapsedTime.text = [Utility stringFromTimeInterval:elapsedTime];//[formatter stringFromDate:elapsedTimeDate];
//            
//            NSDate *timeRemainingDate = [NSDate dateWithTimeIntervalSinceNow:timeRemaining];
//            _timeRemaining.text = [Utility stringFromTimeInterval:timeRemaining];//[formatter stringFromDate:timeRemainingDate];
            
//            _slider.value = percentage * 0.01;
            
            if(finished)
                [self playNextSongs];
            
        } else {
            
            NSLog(@"There has been an error playing the remote file: %@", [error description]);
        }
        
    }];
}


-(void)playNextSongs
{
//    if(isRepeat)
//    {
//        [[HRSoundManager sharedManager] restart];
//    }
//    else if(isShuffle)
//    {
//        int count = [[self getSongsList] count];
//        
//        int randNum = rand() % count;
//        
//        self.currSongIndex = randNum;
//        [self buildSongsUI];
//    }
//    else
//    {
//        [self btnNextClicked:Nil];
//    }
}


-(void)backOrForwardAudio:(UISlider *)sender {
    
    [[HRSoundManager sharedManager] moveToSection:sender.value];
}

-(void)pauseAudio {
    [[HRSoundManager sharedManager]pause];
}

-(void)resumeAudio {
    [[HRSoundManager sharedManager]resume];
}

-(void)stopAudio {
    [[HRSoundManager sharedManager] stop];
}


@end
