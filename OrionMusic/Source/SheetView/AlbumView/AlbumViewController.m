//
//  ChartsViewViewController.m
//  Orin
//
//  Created by Finbarrs Oketunji on 10/18/13.
//
//

#import "AlbumViewController.h"
#import "GameUtility.h"
#import "AHKActionSheet.h"
#import "HRSoundManager.h"
#import "AlbumDtlsViewController.h"
#import "AppDelegate.h"

@interface AlbumViewController ()
{
    NSInteger m_nCurCount;
    NSInteger selectedAlbum;
    NSInteger selectedAlbumSong;
}
@end

@implementation AlbumViewController

@synthesize parentController;
@synthesize arrAlbumItems;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"chart view: %f, %f, %f, %f", self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    
    [GameUtility setPhoneFrame:self.view withHeader:YES];
    
    self.viewPlay.hidden = YES;
	//[_m_chartView createItemView:self];
	//_m_chartView.m_scrollView.delegate = self;
//	[self createHeaderView];
////	[self testFinishedLoadData];
//	[self performSelector:@selector(testFinishedLoadData) withObject:nil afterDelay:0.0f];

    [self.gridView registerNib:[UINib nibWithNibName:@"GridItemCell" bundle:nil] forCellWithReuseIdentifier:@"GridItemCell"];
    
    [self loadMusic:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self stopAudio];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setM_chartView:nil];
    [self stopAudio];
    [super viewDidUnload];
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

#pragma gridview

-(void)updateUI
{
    [self.gridView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [self.arrAlbumItems count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:@"GridItemCell"
                                              forIndexPath:indexPath];
    
    //cell.layer.borderWidth = 1.0f;
    //cell.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    //
    NSDictionary *item = [self.arrAlbumItems objectAtIndex:indexPath.row];
    
    NSString *name = item[@"album_title"];
    
    NSDictionary *itemSong = [self getAlbumSongItem:indexPath.row songIndex:0];


    NSString *artiest = @"Unknown";
    NSString *imagefile = @"";

    if(itemSong)
    {
        artiest = itemSong[@"artist"];
        imagefile = itemSong[@"imagefile"];
    }

    //
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1000];
    UILabel *lb1 = (UILabel *)[cell viewWithTag:1001];
    UILabel *lb2 = (UILabel *)[cell viewWithTag:1002];
    UILabel *lb3 = (UILabel *)[cell viewWithTag:1003];
    
    //
    NSString* strUrl = [NSString stringWithFormat:@"%@/album/%@", SERVER_PATH, imagefile];
    [GameUtility setImageFromUrl:strUrl target:imageView defaultImg:@""];

    lb1.text = name;
    lb2.text = artiest;
    lb3.text = [NSString stringWithFormat:@"Songs %d", [[self getAlbumSongs:indexPath.row] count]];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedAlbum = indexPath.row;
    [self showAlbumSongs:indexPath.row];
    
//    NSDictionary *item = [self.arrAlbumItems objectAtIndex:indexPath.row];
//    NSString *songfile = item[@"songfile"];
//
//    NSString* ituneFile = [NSString stringWithFormat:@"%@/song/%@", SERVER_PATH, [GameUtility urlencode:songfile]];
//    
//    [self playRemoteFile:ituneFile];
}


- (void)showAlbumSongs:(NSInteger)index
{
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

    NSDictionary *item = [self.arrAlbumItems objectAtIndex:index];

    AlbumDtlsViewController *vc = [[AlbumDtlsViewController alloc] initWithNibName:@"AlbumDtlsViewController" bundle:nil data:item];
    
    [appDelegate.navigationController pushViewController:vc animated:YES];
    
//    AHKActionSheet *actionSheet = [[AHKActionSheet alloc] initWithTitle:nil];
//    
//    actionSheet.blurTintColor = [UIColor colorWithWhite:0.0f alpha:0.55f];
//    actionSheet.blurRadius = 8.0f;
//    actionSheet.buttonHeight = 50.0f;
//    actionSheet.cancelButtonHeight = 50.0f;
//    actionSheet.animationDuration = 0.5f;
//    actionSheet.cancelButtonShadowColor = [UIColor colorWithWhite:0.0f alpha:0.1f];
//    actionSheet.separatorColor = [UIColor colorWithWhite:1.0f alpha:0.3f];
//    actionSheet.selectedBackgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
//    
//    UIFont *defaultFont = [UIFont boldSystemFontOfSize:17.0f];//fontWithName:@"Avenir" size:17.0f];
//    actionSheet.buttonTextAttributes = @{ NSFontAttributeName : defaultFont,
//                                          NSForegroundColorAttributeName : [UIColor whiteColor] };
//    actionSheet.disabledButtonTextAttributes = @{ NSFontAttributeName : defaultFont,
//                                                  NSForegroundColorAttributeName : [UIColor grayColor] };
//    actionSheet.destructiveButtonTextAttributes = @{ NSFontAttributeName : defaultFont,
//                                                     NSForegroundColorAttributeName : [UIColor redColor] };
//    actionSheet.cancelButtonTextAttributes = @{ NSFontAttributeName : defaultFont,
//                                                NSForegroundColorAttributeName : [UIColor whiteColor] };
//    
//    //UIView *headerView = [[self class] fancyHeaderView];
//    //actionSheet.headerView = headerView;
//    
//    //    [actionSheet addButtonWithTitle:NSLocalizedString(@"Info", nil)
//    //                              image:[UIImage imageNamed:@"Icon1"]
//    //                               type:AHKActionSheetButtonTypeDefault
//    //                            handler:nil];
//    //
//    //    [actionSheet addButtonWithTitle:NSLocalizedString(@"Add to Favorites (disabled)", nil)
//    //                              image:[UIImage imageNamed:@"Icon2"]
//    //                               type:AHKActionSheetButtonTypeDisabled
//    //                            handler:nil];
//    
//    for (int i = 0; i < 10; i++) {
//        [actionSheet addButtonWithTitle:[NSString stringWithFormat:@"Share %d", i]
//                                  image:nil
//                                   type:AHKActionSheetButtonTypeDefault
//                                handler:^(AHKActionSheet *actionSheet) {
//                                    selectedAlbumSong = actionSheet.selectedIndex;
//                                    NSLog(@"Selected Item %d", actionSheet.selectedIndex);
//                                    
//                                    [self playSongs];
//                                }];
//    }
//    
//    [actionSheet show];
}

-(NSArray *)getAlbumSongs:(NSInteger)index
{
    NSDictionary *dictItem = [self.arrAlbumItems objectAtIndex:index];
    if(![[dictItem objectForKey:@"album_songs"] isKindOfClass:[NSArray class]])
        return nil;
    
    return [dictItem objectForKey:@"album_songs"];
}


-(NSDictionary *)getAlbumSongItem:(NSInteger)index songIndex:(NSInteger)songIndex
{
    if(![[self getAlbumSongs:index] isKindOfClass:[NSArray class]])
        return nil;
    
    if([[self getAlbumSongs:index] count] == 0) return nil;
    
    NSDictionary *dictItem = [[self getAlbumSongs:index] objectAtIndex:songIndex];
    
    return dictItem;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
#pragma mark
#pragma methods for creating and removing the header view

-(void)createHeaderView{
    if (_refreshHeaderView && [_refreshHeaderView superview]) {
        [_refreshHeaderView removeFromSuperview];
    }
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:
                          CGRectMake(0.0f, 0.0f - self.view.bounds.size.height,
									 self.view.frame.size.width, self.view.bounds.size.height)];
    _refreshHeaderView.delegate = self;
    
	[self.m_chartView.m_scrollView addSubview:_refreshHeaderView];
    
    [_refreshHeaderView refreshLastUpdatedDate];
}

-(void)removeHeaderView{
    if (_refreshHeaderView && [_refreshHeaderView superview]) {
        [_refreshHeaderView removeFromSuperview];
    }
    _refreshHeaderView = nil;
}

-(void)setFooterView{
//    UIEdgeInsets test = self.m_chartView.m_scrollView.contentInset;
    // if the footerView is nil, then create it, reset the position of the footer
    CGFloat height = MAX(self.m_chartView.m_scrollView.contentSize.height, self.m_chartView.m_scrollView.frame.size.height);
    if (_refreshFooterView && [_refreshFooterView superview]) {
        // reset position
        _refreshFooterView.frame = CGRectMake(0.0f,
                                              height,
                                              self.m_chartView.m_scrollView.frame.size.width,
                                              self.view.bounds.size.height);
    }else {
        // create the footerView
        _refreshFooterView = [[EGORefreshTableFooterView alloc] initWithFrame:
                              CGRectMake(0.0f, height,
                                         self.m_chartView.m_scrollView.frame.size.width, self.view.bounds.size.height)];
        _refreshFooterView.delegate = self;
        [self.m_chartView.m_scrollView addSubview:_refreshFooterView];
    }
    
    if (_refreshFooterView) {
        [_refreshFooterView refreshLastUpdatedDate];
    }
}

-(void)removeFooterView{
    if (_refreshFooterView && [_refreshFooterView superview]) {
        [_refreshFooterView removeFromSuperview];
    }
    _refreshFooterView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

#pragma mark-
#pragma mark force to show the refresh headerView
-(void)showRefreshHeader:(BOOL)animated{
	if (animated)
	{
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.2];
		self.m_chartView.m_scrollView.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
        // scroll the table view to the top region
        [self.m_chartView.m_scrollView scrollRectToVisible:CGRectMake(0, 0.0f, 1, 1) animated:NO];
        [UIView commitAnimations];
	}
	else
	{
        self.m_chartView.m_scrollView.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
		[self.m_chartView.m_scrollView scrollRectToVisible:CGRectMake(0, 0.0f, 1, 1) animated:NO];
	}
    
    [_refreshHeaderView setState:EGOOPullRefreshLoading];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	//	NSLog(@"scrollViewDidScroll");
	
	if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
	
	if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	NSLog(@"scrollViewDidEndDragging");
	
	if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
	
	if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
    }
}

//===============
//刷新delegate
#pragma mark -
#pragma mark data reloading methods that must be overide by the subclass

-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
	
	//  should be calling your tableviews data source model to reload
	_reloading = YES;
    
    if (aRefreshPos == EGORefreshHeader) {
        // pull down to refresh data
        [self performSelector:@selector(refreshView) withObject:nil afterDelay:2.0];
    }else if(aRefreshPos == EGORefreshFooter){
        // pull up to load more data
        [self performSelector:@selector(getNextPageView) withObject:nil afterDelay:0.0];
    }
	
	// overide, the actual loading data operation is done in the subclass
}

#pragma mark -
#pragma mark method that should be called when the refreshing is finished
- (void)finishReloadingData{
	
	NSLog(@"finishReloadingData");
	
	//  model should call this when its done loading
	_reloading = NO;
    
	if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.m_chartView.m_scrollView];
    }
    
    if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.m_chartView.m_scrollView];
        [self setFooterView];
    }
    
    // overide, the actula reloading tableView operation and reseting position operation is done in the subclass
}


#pragma mark -
#pragma mark EGORefreshTableDelegate Methods

- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos{
	
	NSLog(@"egoRefreshTableDidTriggerRefresh");
	
	[self beginToReloadData:aRefreshPos];
	
}

- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}


// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view{
	
	NSLog(@"egoRefreshTableDataSourceLastUpdated");
	
	return [NSDate date]; // should return date data source was last changed
	
}

//刷新调用的方法
-(void)refreshView{
//    DataAccess *dataAccess= [[DataAccess alloc]init];
//    NSMutableArray *dataArray = [dataAccess getDateArray];
//    [self.aoView refreshView:dataArray];
    [self testFinishedLoadData];
	
}
//加载调用的方法
-(void)getNextPageView{
	[self.m_chartView loadMusic:NO];
	
//    [self testFinishedLoadData];
	
}

-(void)testFinishedLoadData{
    [self finishReloadingData];
    [self setFooterView];
}


- (void)loadMusic: (BOOL)bShowWaiting {
	GameUtility* utils = [GameUtility sharedObject];
    
    int nGetNum = 10;//GETMUSICNUM;
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait) {
        nGetNum *= 4;
    }
    
    //http://orin.io/webservice/webservice.php?type=album_song
    
	NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
						 utils.userProfile.userName, @"username",
						 nil];

//    NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
//						 utils.userProfile.userName, @"username",
//                         [NSString stringWithFormat:@"%d", m_nCurCount], @"currentnum",
//                         [NSString stringWithFormat:@"%d", nGetNum], @"getcount",
//						 nil];

	if (bShowWaiting)
		[utils runAlbumWebService:@"album_song" Param:dic Delegate:self View:self.view];
	else
		[utils runAlbumWebService:@"album_song" Param:dic Delegate:self View:nil];
}


- (void)finishedRunningServiceDelegate:(NSDictionary *)outData {
    
    //	GameUtility* utils = [GameUtility sharedObject];
    
    NSArray *resultArr = (NSArray *)outData;
    
	NSLog(@"receivedJson: %@", outData);
//	NSString* serviceName = outData[@"serviceName"];
	
	if([resultArr count] == 0) {
		return;
	}
    
    NSDictionary *dictItem = [resultArr objectAtIndex:0];
	int count = 0;
	if ([[dictItem objectForKey:@"status"] intValue] == 1) {
        
        if(self.arrAlbumItems)
            [self.arrAlbumItems removeAllObjects];
        else
            self.arrAlbumItems = [NSMutableArray new];
        
        for(NSDictionary *dict in resultArr)
        {
            if(count++ == 0) continue;
            
            [self.arrAlbumItems addObject:dict];
        }
        
        [self updateUI];
        
        //        NSInteger nCount = -1;
        //
        //        m_nCurCount += (nCount + 1);
		
        /*
		for(id item in items) {
			
			AlbumSongData* data = [AlbumSongData songData];
			data.url = item[@"itunelink"];
			data.title = item[@"name"];
            data.artist = item[@"artist"];
			data.pubDate = item[@"time"];
			data.likeCount = [item[@"likes"] integerValue];
			data.commentCount = [item[@"comments"] integerValue];
			data.strImage = item[@"imagefile"];
			data.nIndex = [item[@"id"] integerValue];
			data.strSong = item[@"songfile"];
            
			AlbumsItemView* itemView = [AlbumsItemView chartsItemView:data];
			[itemView setIsLiked:[item[@"liked"] boolValue]];
			itemView.tag = 1000 + m_nCurCount;
            
            if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
                itemView.frame = CGRectMake(0, 340 * (m_nCurCount), 320, 340);
            }
            else {
                itemView.frame = CGRectMake(25 + 330 * (m_nCurCount % 3), 25 + 340 * (m_nCurCount / 3), 330, 340);
            }
            
			[m_scrollView addSubview:itemView];
			
			[itemView setLikedUsers : (NSArray *)item[@"likedusers"]];
            
            m_nCurCount++;
		}
        
        //        m_nCurCount += (nCount + 1);
		
		CGSize sz = m_scrollView.frame.size;
        
        if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
            sz.height = 340 * m_nCurCount;
        }
        else {
            //            if (m_nCurCount == GETMUSICNUM)
            //                m_nCurCount += GETMUSICNUM;
            sz.height = 25 + 340 * m_nCurCount / 3;
        }
		
		[m_scrollView setContentSize:sz];
        
		
		[m_parent performSelector:@selector(testFinishedLoadData) withObject:nil afterDelay:0.0f];
        */
	}
}

#pragma --
#pragma playSongs

-(void)playSongs
{

    NSDictionary *item = [self.arrAlbumItems objectAtIndex:2];
    NSString *songfile = item[@"songfile"];
    
    NSString* ituneFile = [NSString stringWithFormat:@"%@/song/%@", SERVER_PATH, [GameUtility urlencode:songfile]];
    
    [self playRemoteFile:ituneFile];
    
    //
    NSString *name = item[@"name"];
    NSString *artiest = item[@"artist"];
    NSString *imagefile = item[@"imagefile"];
    
    //
    NSString* strUrl = [NSString stringWithFormat:@"%@/song/image/%@", SERVER_PATH, imagefile];
    [GameUtility setImageFromUrl:strUrl target:self.imageViewCover defaultImg:@""];

    self.lbTitle.text = name;
    self.lbArtist.text = artiest;
}

- (IBAction)btnPlayClicked:(id)sender {

    if(self.btnPlay.selected)
    {
        [self pauseAudio];
        self.btnPlay.selected = NO;
    }
    else
    {
        self.btnPlay.selected = YES;

        [self resumeAudio];
    }
}


#pragma --
#pragma playRemoteFile

-(void)playRemoteFile:(NSString *)songLink {
    
    self.viewPlay.hidden = NO;

    if([[HRSoundManager sharedManager] isPlaying])
        [[HRSoundManager sharedManager] stop];
    
    self.btnPlay.selected = YES;
    
    [[HRSoundManager sharedManager] startStreamingRemoteAudioFromURL:songLink andBlock:^(int percentage, CGFloat elapsedTime, CGFloat timeRemaining, NSError *error, BOOL finished) {
        
        if (!error) {
            
           // NSLog(@"music update %f, %f, %d", elapsedTime, timeRemaining, finished);
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"mm:ss"];
            
            NSDate *elapsedTimeDate = [NSDate dateWithTimeIntervalSinceNow:elapsedTime];
//            _elapsedTime.text = [Utility stringFromTimeInterval:elapsedTime];//[formatter stringFromDate:elapsedTimeDate];
//            
//            NSDate *timeRemainingDate = [NSDate dateWithTimeIntervalSinceNow:timeRemaining];
//            _timeRemaining.text = [Utility stringFromTimeInterval:timeRemaining];//[formatter stringFromDate:timeRemainingDate];
            
//            _slider.value = percentage * 0.01;
            
            if(finished)
                [self playNextSongs];
            
        } else {
            
            NSLog(@"There has been an error playing the remote file: %@", [error description]);
        }
        
    }];
}


-(void)playNextSongs
{
//    if(isRepeat)
//    {
//        [[HRSoundManager sharedManager] restart];
//    }
//    else if(isShuffle)
//    {
//        int count = [[self getSongsList] count];
//        
//        int randNum = rand() % count;
//        
//        self.currSongIndex = randNum;
//        [self buildSongsUI];
//    }
//    else
//    {
//        [self btnNextClicked:Nil];
//    }
}


-(void)backOrForwardAudio:(UISlider *)sender {
    
    [[HRSoundManager sharedManager] moveToSection:sender.value];
}

-(void)pauseAudio {
    [[HRSoundManager sharedManager]pause];
}

-(void)resumeAudio {
    [[HRSoundManager sharedManager]resume];
}

-(void)stopAudio {
    [[HRSoundManager sharedManager] stop];
}


@end
